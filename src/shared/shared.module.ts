import { Module } from '@nestjs/common';
import { AuthHelperService } from './auth-helper/auth-helper.service';

@Module({
  imports: [AuthHelperService],
  providers: [AuthHelperService]
})
export class SharedModule {}
