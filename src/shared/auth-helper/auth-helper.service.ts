import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { getConnection } from 'typeorm';

@Injectable()
export class AuthHelperService {

  async runAwaitGetAll(repository): Promise<any[]> {
    try {
      return await repository.find();
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

  async runAwaitGet(repository, dto, id): Promise<any> {
    try {
      const obj = await repository.findOne(id)
      if(!obj) throw new NotFoundException();
      return plainToClass(dto, obj);
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

  async runAwaitPost(repository, dto, obj): Promise<any> {
    try {
      return plainToClass(dto, await repository.save(obj));
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

  async runAwaitPath(repository, dto, obj, id): Promise<any> {
    try {
      return await repository.update(id, obj);
      return this.runAwaitGet(repository, dto, id);
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

  async runAwaitDelete(repository, ids): Promise<boolean> {
    try {
      if(ids.length > 0) ids = ids.split(',');
      return !!await repository.delete(ids);
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

  async runAwaitRelationQueryPost(q, r, qObj, rObj): Promise<any> {
    try {
      return await getConnection()
        .createQueryBuilder()
        .relation(q, r)
        .of(qObj)
        .add(rObj);
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

  async runAwaitRelationQueryDelete(q, r, qObj, rObj): Promise<any>{
    try {
      return await getConnection()
        .createQueryBuilder()
        .relation(q, r)
        .of(qObj)
        .remove(rObj);
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

}
