import { Module } from '@nestjs/common';
import { UserModule } from './modules/user/user.module';
import { RoleModule } from './modules/role/role.module';
import { ConfigService } from './config/config.service';
import { Configuration } from './config/config.keys';
import { ConfigModule } from './config/config.module';
import { DatabaseModule } from './database/database.module';
import { AuthModule } from './modules/auth/auth.module';
import { MeetingModule } from './modules/meeting/meeting.module';
import { EventModule } from './modules/event/event.module';
import { AvailabilityModule } from './modules/availability/availability.module';
import { RoomModule } from './modules/room/room.module';
import { QuestionModule } from './modules/question/question.module';
import { OptionModule } from './modules/option/option.module';
import { NotificationModule } from './modules/notification/notification.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { UserNotificationModule } from './modules/user-notification/user-notification.module';
import { UserMeetingModule } from './modules/user-meeting/user-meeting.module';
require('dotenv').config()

@Module({
  providers: [],
  imports: [
    ConfigModule,
    DatabaseModule,
    UserModule,
    RoleModule,
    AuthModule,
    MeetingModule,
    EventModule,
    AvailabilityModule,
    RoomModule,
    QuestionModule,
    OptionModule,
    NotificationModule,
    UserNotificationModule,
    UserMeetingModule,
    MailerModule.forRoot({
      transport: {
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        secure: false, // true for 465, false for other ports
        auth: {
          user: process.env.EMAIL_ID, // generated ethereal user
          pass: process.env.EMAIL_PASS // generated ethereal password
        },
        tls: {
          rejectUnauthorized: false
        }
      },
      defaults: {
        from: '"nest-modules" <noreply@negular.com>', // outgoing email ID
      },
      /*template: {
        dir: process.cwd() + '/template/',
        adapter: new HandlebarsAdapter(), // or new PugAdapter()
        options: {
          strict: true,
        },
      },*/
    }),
  ],
})
export class AppModule {
  static port: number | string;

  constructor(private readonly _configService: ConfigService) {
    AppModule.port = this._configService.get(Configuration.PORT);
  }
}
