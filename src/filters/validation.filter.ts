import { ArgumentsHost, Catch, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { ValidationException } from './validation.exception';
import { Request } from 'express';

@Catch(ValidationException)
export class ValidationFilter implements ExceptionFilter {
  catch(exception: ValidationException, host: ArgumentsHost): any {
    const ctx = host.switchToHttp(),
      response = ctx.getResponse();
    const request = ctx.getRequest<Request>();

    return response.status(400).json({
      statusCode: HttpStatus.BAD_REQUEST,
      createdBy: 'ValidationFilter',
      errors: exception.validationError,
      timestamp: new Date().toISOString(),
      path: request.url,
    })
  }
}
