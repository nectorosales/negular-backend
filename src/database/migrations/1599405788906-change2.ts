import {MigrationInterface, QueryRunner} from "typeorm";

export class change21599405788906 implements MigrationInterface {
    name = 'change21599405788906'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `rooms` CHANGE `user_id` `user_id` int NULL");
        await queryRunner.query("ALTER TABLE `rooms` ADD CONSTRAINT `FK_ebd4d9c6a68f360a13a2f5a3ad3` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `rooms` DROP FOREIGN KEY `FK_ebd4d9c6a68f360a13a2f5a3ad3`");
        await queryRunner.query("ALTER TABLE `rooms` CHANGE `user_id` `user_id` int NOT NULL");
    }

}
