import { Factory, Seeder } from 'typeorm-seeding'
import { Connection } from 'typeorm'
import { Role } from '../../modules/role/role.entity';

export default class RoleSeeder implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await connection
      .createQueryBuilder()
      .insert()
      .into(Role)
      .values([
        { name: 'ADMIN', description: 'Rol de Administrador' },
        { name: 'USER', description: 'Rol de Usuario' },
        { name: 'CLIENT', description: 'Rol de Cliente' },
      ])
      .execute()
  }
}
