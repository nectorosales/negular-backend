import {TypeOrmModule } from '@nestjs/typeorm'
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { ConnectionOptions } from 'typeorm';
import { Configuration } from '../config/config.keys';

export const databaseProviders = [
  TypeOrmModule.forRootAsync({
    imports: [ConfigModule],
    inject: [ConfigService],
     async useFactory(config: ConfigService){
      return {
        //ssl: true,
        type: 'mysql',
        port: 3306,
        host: config.get(Configuration.HOST),
        username: config.get(Configuration.MYSQL_USER),
        password: config.get(Configuration.MYSQL_PASSWORD),
        database: config.get(Configuration.MYSQL_DATABASE),
        entities: [__dirname + '/../**/*.entity{.ts,.js}'],
        migrations: [__dirname + '/migrations/*{.ts,.js}'],
        seeds: [__dirname + '/seeds/!*{.ts,.js}'],
        synchronize: false,
      } as ConnectionOptions
     }
  })
]
