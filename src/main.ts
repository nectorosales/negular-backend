import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './filters/http-exception.filter';
import { ValidationPipe } from '@nestjs/common';
import { ValidationError } from 'class-validator';
import { ValidationFilter } from './filters/validation.filter';
import { ValidationException } from './filters/validation.exception';
import * as fs from 'fs';
import { ExpressAdapter } from '@nestjs/platform-express';
import express = require('express');
import http = require("http");
import https = require("https");
import { join } from 'path';

async function bootstrap() {
  let httpsOptions = {
    ca: fs.readFileSync('./certificates/ca_certificate.crt'),
    key: fs.readFileSync('./certificates/key.pem'),
    cert: fs.readFileSync('./certificates/certificate.crt')
  };
  const server = express();
  const app = await NestFactory.create(AppModule, new ExpressAdapter(server));
  app.useGlobalFilters(new HttpExceptionFilter(), new ValidationFilter());
  app.useGlobalPipes(new ValidationPipe({
    skipMissingProperties: true,
    exceptionFactory: (errors:  ValidationError[]) => {
      const messages = errors.map(
        error => `${error.property} has wrong value ${error.value},
        ${Object.values(error.constraints).join(', ')}`
      )
      return new ValidationException(messages);
    }
  }));
  app.enableCors();
  app.setGlobalPrefix('api');
  app.use('/public', express.static(join(__dirname, '..', 'public')));

  await app.init();

  http.createServer(server).listen(3000);
  https.createServer(httpsOptions, server).listen(3443, () => {
    console.log('Listening...')
  });
}
bootstrap();
