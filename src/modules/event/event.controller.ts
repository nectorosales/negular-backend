import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { EventService } from './event.service';
import { Event } from './event.entity';
import { EventDto } from './dto/event.dto';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../role/decorators/role.decorator';
import { RoleGuard } from '../role/guards/role.guard';
import { RoleType } from '../role/roletype.enum';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';

@Controller('events')
export class EventController {
  constructor(private readonly _eventService: EventService) {}

  @Get()
  async getEvents(): Promise<EventDto[]>{
    return await this._eventService.getEvents();
  }

  @Get('username/:username/:slug')
  async getEventByUsername(@Param() params: any): Promise<EventDto> {
    return await this._eventService.getEventByUsername(params);
  }

  @Get('user/:id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getEventsByUser(@Param() idUser: number): Promise<EventDto[]>{
    return await this._eventService.getAllByUser(idUser);
  }

  @Get(':id')
  @UseGuards(AuthGuard(), RoleGuard)
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getEvent(@Param('id') id: string): Promise<EventDto> {
    return await this._eventService.get(id);
  }

  @Post()
  @UseInterceptors(FileInterceptor('img', {
    storage: diskStorage({
      destination: './public'
      , filename: (req, file, cb) => {
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
        cb(null, `${randomName}${extname(file.originalname)}`)
      }
    })
  }))
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async create(@UploadedFile() file, @Body() event: Event): Promise<EventDto> {
    event.img = file.filename;
    event.active = event.active ? 1 : 0;
    return await this._eventService.store(event);
  }

  @Put(':id')
  @UseInterceptors(FileInterceptor('img', {
    storage: diskStorage({
      destination: './public'
      , filename: (req, file, cb) => {
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
        cb(null, `${randomName}${extname(file.originalname)}`)
      }
    })
  }))
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async update(@Param('id') id: string, @UploadedFile() file, @Body() event: Event) {
    if(file) {
      event.img = file.filename;
    }
    event.active = event.active ? 1 : 0;
    return await this._eventService.update(id, event);
  }

  @Delete()
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async delete(@Query() query) {
    return this._eventService.destroy(query.ids);
  }
}
