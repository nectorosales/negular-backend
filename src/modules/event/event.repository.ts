import { EntityRepository, getConnection, Repository } from 'typeorm';
import { Event } from './event.entity';

@EntityRepository(Event)
export class EventRepository extends Repository<Event>{
  getAllByUser(idUser: number): Promise<Event[]> {
    return getConnection()
      .createQueryBuilder()
      .select("event")
      .from(Event, "event")
      .where({user: idUser})
      .leftJoinAndSelect("event.user", "user")
      .leftJoinAndSelect("event.availabilities", "availability")
      .leftJoinAndSelect("event.questions", "question")
      .leftJoinAndSelect("question.options", "option", "option.question = question.id")
      .addOrderBy("event.createdAt", "ASC")
      .addOrderBy("option.order", "ASC")
      .addOrderBy("question.order", "ASC")
      .addOrderBy("availability.begin", "ASC")
      .getMany();
  }

  async getEventByUsername(params: any) {
    const {username, slug} = params;
    const result = getConnection()
      .createQueryBuilder()
      .select("event")
      .from(Event, "event")
      .where({slug: slug})
      .leftJoinAndSelect("event.user", "user")
      //.where("user.username = :userName", {userName: username})
      .leftJoinAndSelect("event.availabilities", "availability")
      .leftJoinAndSelect("event.questions", "question")
      .leftJoinAndSelect("question.options", "option", "option.question = question.id")
      .addOrderBy("event.createdAt", "ASC")
      .addOrderBy("option.order", "ASC")
      .addOrderBy("question.order", "ASC")
      .addOrderBy("availability.begin", "ASC")
      .getOne();
    return result;
  }
}
