import { IsBoolean, IsDecimal, IsNotEmpty, IsNumber, MaxLength } from 'class-validator';
import { User } from '../../user/user.entity';
import { Meeting } from '../../meeting/meeting.entity';
import { Availability } from '../../availability/availability.entity';
import { Question } from '../../question/question.entity';

export class EventDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  img: string;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  slug: string;

  @IsNotEmpty()
  typeEvent: string;

  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  colour: string;

  @IsNotEmpty()
  @IsNumber()
  @MaxLength(10)
  duration: number;

  @IsNotEmpty()
  @IsDecimal()
  price: number;

  @IsNotEmpty()
  @IsBoolean()
  active: boolean;

  @IsNotEmpty()
  meeting: Meeting[];

  @IsNotEmpty()
  user: User;

  @IsNotEmpty()
  availabilities: Availability[]

  @IsNotEmpty()
  questions: Question[]
}
