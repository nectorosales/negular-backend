import { Injectable } from '@nestjs/common';
import { EventRepository } from './event.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { EventDto } from './dto/event.dto';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';
import { Event } from './event.entity';
import { plainToClass } from 'class-transformer';
import { MeetingDto } from '../meeting/dto/meeting.dto';

@Injectable()
export class EventService {
  constructor(
    @InjectRepository(EventRepository)
    private readonly _eventRepository: EventRepository,
    private readonly _authHelperService: AuthHelperService,
  ) {}

  async getEvents(): Promise<EventDto[]>{
    return this._authHelperService.runAwaitGetAll(this._eventRepository);
  }

  async getAllByUser(idUser: number): Promise<EventDto[]>{
    return plainToClass(EventDto, await this._eventRepository.getAllByUser(idUser));
  }

  async get(id: string): Promise<EventDto>{
    return this._authHelperService.runAwaitGet(this._eventRepository, Event, id);
  }

  async store(event: Partial<Event>): Promise<EventDto>{
    return this._authHelperService.runAwaitPost(this._eventRepository, Event, event);
  }

  async update(id: string, event: Partial<Event>): Promise<EventDto>{
    return plainToClass(EventDto, await this._eventRepository.save(event));
    //return this._authHelperService.runAwaitPath(this._eventRepository, Event, event, id);
  }

  async destroy(ids: Array<number>) {
    return this._authHelperService.runAwaitDelete(this._eventRepository, ids);
  }

  async getEventByUsername(params: any): Promise<EventDto> {
    return plainToClass(EventDto, await this._eventRepository.getEventByUsername(params));
  }
}
