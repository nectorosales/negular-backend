import { Module } from '@nestjs/common';
import { EventRepository } from './event.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventService } from './event.service';
import { SharedModule } from '../../shared/shared.module';
import { EventController } from './event.controller';
import { AuthModule } from '../auth/auth.module';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([EventRepository]),
    SharedModule,
    AuthModule
  ],
  providers: [
    EventService,
    AuthHelperService
  ],
  controllers: [
    EventController
  ]
})
export class EventModule {}
