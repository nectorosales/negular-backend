import {
  BaseEntity,
  Column, CreateDateColumn, DeleteDateColumn,
  Entity, JoinColumn,
  ManyToOne, OneToMany,
  PrimaryGeneratedColumn, UpdateDateColumn,
} from 'typeorm';
import { Meeting } from '../meeting/meeting.entity';
import { User } from '../user/user.entity';
import { Availability } from '../availability/availability.entity';
import { Question } from '../question/question.entity';

@Entity('events')
export class Event extends BaseEntity {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({type: 'varchar', length: 255, nullable: false})
  name: string;

  @Column({type: 'varchar', length: 255, nullable: false})
  slug: string;

  @Column({type: 'varchar', length: 255, nullable: false})
  img: string;

  @Column({type: 'varchar', length: 25, nullable: false, default: 'single'})
  typeEvent: string;

  @Column({type: 'varchar', length: 500, nullable: false})
  description: string;

  @Column({type: 'varchar', length: 25, nullable: false})
  colour: string;

  @Column({type: 'int', nullable: false})
  duration: number;

  @Column({type: 'decimal', precision: 5, scale: 2, nullable: false, default: 0})
  price: number;

  @Column({type: 'boolean', nullable: false, default: false})
  active: number;

  @OneToMany(type => Meeting, meeting => meeting.event)
  meetings: Meeting[];

  @ManyToOne(type => User)
  @JoinColumn({name: 'user_id'})
  user: User;

  @OneToMany(type => Availability, availability => availability.event,{eager: true})
  availabilities: Availability[];

  @OneToMany(type => Question, question => question.event,{eager: true})
  questions: Question[];

  @CreateDateColumn({type: 'timestamp', name: 'created_at'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamp', name: 'updated_at'})
  updatedAt: Date;

  @DeleteDateColumn({type: 'timestamp', name: 'deleted_at'})
  deletedAt: Date;
}
