import { EntityRepository, getConnection, Repository } from 'typeorm';
import { User } from '../user/user.entity';
import { SignupDto } from './dto/signup.tdo';
import { RoleRepository } from '../role/role.repository';
import { Role } from '../role/role.entity';
import { RoleType } from '../role/roletype.enum';
import { UserDetails } from '../user/user.details.entity';
import { genSalt, hash } from 'bcryptjs';
import { ConflictException } from '@nestjs/common';

@EntityRepository(User)
export class AuthRepository extends Repository<User> {

  async signup(signupDto: SignupDto) {

    const {email, username} = signupDto;
    const user = new User();
    user.email = email;
    user.username = username;

    const roleRepository: RoleRepository = await getConnection().getRepository(
      Role,
    );

    const defaultRole: Role = await roleRepository.findOne({where: { name: RoleType.USER}})

    user.roles = [defaultRole];

    return this.createDetailsAndPassword(signupDto, user);
  }

  async signupUserExists(signupDto: SignupDto, userExists) {

    const roleRepository: RoleRepository = await getConnection().getRepository(
      Role,
    );

    const defaultRole: Role = await roleRepository.findOne({where: { name: RoleType.USER}})

    if(!userExists.roles.filter(r => r.id === defaultRole.id)[0]) {
      userExists.roles.push(defaultRole);
    } else {
      throw new ConflictException("email o username already exists");
    }

    return this.createDetailsAndPassword(signupDto, userExists);
  }

  async createDetailsAndPassword(signupDto, user){
    const {img, name, lastName, phone, password} = signupDto;
    const details = new UserDetails();
    details.img = img;
    details.name = name;
    details.lastName = lastName;
    details.phone = phone;
    user.details = details;

    const salt = await genSalt(10);
    user.password = await hash(password, salt);

    return await user.save();
  }

}
