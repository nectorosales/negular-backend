import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { AuthRepository } from './auth.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { SignupDto } from './dto/signup.tdo';
import { SigninDto } from './dto/signin.dto';
import { User } from '../user/user.entity';
import { compare } from 'bcryptjs';
import { IJwtPayload } from './jwt-payload.interface';
import { RoleType } from '../role/roletype.enum';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(AuthRepository)
    private readonly _authRepository: AuthRepository,
    private readonly _jwtService: JwtService,
    ) {}

    async signup(signupDto: SignupDto) {

      const {email, username} = signupDto;
      const userExists = await this._authRepository.findOne({
        where: [{email}, {username}],
      });

      let user = {
        id: '', email: '', roles: [],
      };

      if(userExists) {
        user = await this._authRepository.signupUserExists(signupDto, userExists);
      } else {
        user = await this._authRepository.signup(signupDto);
      }
      const payload: IJwtPayload = {
        id: user.id,
        email: user.email,
        roles: user.roles.map(role => role.name as RoleType)
      }

      const token = await this._jwtService.sign(payload);
      return { token };

    }

    async signin(signinDto: SigninDto) {
      const {email, password} = signinDto;
      const user: User = await this._authRepository.findOne({
        where: { email },
      });

      if(!user) {
        throw new NotFoundException("user does not exist")
      }

      const isMatch = await compare(password, user.password);

      if(!isMatch) {
        throw new UnauthorizedException("invalid credentials");
      }

      const payload: IJwtPayload = {
        id: user.id,
        email: user.email,
        roles: user.roles.map(role => role.name as RoleType)
      }

      const token = await this._jwtService.sign(payload);
      return { token };
    }
}
