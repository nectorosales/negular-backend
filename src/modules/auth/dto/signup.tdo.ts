import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class SignupDto {

  @IsNotEmpty()
  @IsString()
  img: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  username: string;

  @IsString()
  lastName: string;

  @IsNumber()
  phone: number;

  @IsNotEmpty()
  @IsString()
  email: string;

  @IsNotEmpty()
  @IsString()
  password: string;

}
