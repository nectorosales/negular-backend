import { Body, Controller, Post, UploadedFile, UseInterceptors, UsePipes, ValidationPipe } from '@nestjs/common';
import { SignupDto } from './dto/signup.tdo';
import { AuthService } from './auth.service';
import { SigninDto } from './dto/signin.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { extname } from "path";
import { diskStorage } from 'multer';

@Controller('auth')
export class AuthController {

  constructor(
    private readonly _authService: AuthService
  ){}

  @Post('/signup')
  @UseInterceptors(FileInterceptor('img', {
    storage: diskStorage({
      destination: './public'
      , filename: (req, file, cb) => {
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
        cb(null, `${randomName}${extname(file.originalname)}`)
      }
    })
  }))
  @UsePipes(ValidationPipe)
  async signup(@UploadedFile() file, @Body() signupDto) {
    signupDto.img = file.filename;
    signupDto.phone = parseInt(signupDto.phone);
    return this._authService.signup(signupDto);
  }

  @Post('/signin')
  @UsePipes(ValidationPipe)
  async signin(@Body() signinDto: SigninDto) {
    return this._authService.signin(signinDto);
  }
}
