import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Notification } from '../notification/notification.entity';
import { Meeting } from '../meeting/meeting.entity';
import { User } from '../user/user.entity';

@Entity('user_notifications')
export class UserNotification {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({type: 'varchar', length: 255, nullable: false})
  subject: string;

  @Column({type: 'varchar', length: 500, nullable: false})
  body: string;

  @JoinColumn()
  @ManyToOne(type => User, user => user.userNotifications, {onDelete: 'CASCADE'})
  user: User;

  @JoinColumn()
  @ManyToOne(type => Notification, notification => notification.userNotifications, {eager: true, onDelete: 'CASCADE'})
  notification: Notification;

  @JoinColumn()
  @ManyToOne(type => Meeting, meeting => meeting.userNotifications, {onDelete: 'CASCADE'})
  meeting: Meeting;

  @CreateDateColumn({type: 'timestamp', name: 'created_at'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamp', name: 'updated_at'})
  updatedAt: Date;

  @DeleteDateColumn({type: 'timestamp', name: 'deleted_at'})
  deletedAt: Date;
}
