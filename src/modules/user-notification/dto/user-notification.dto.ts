import { IsNotEmpty, IsString } from 'class-validator';
import { Meeting } from '../../meeting/meeting.entity';
import { User } from '../../user/user.entity';
import { Notification } from '../../notification/notification.entity';

export class UserNotificationDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  @IsString()
  subject: string;

  @IsNotEmpty()
  @IsString()
  body: string;

  @IsNotEmpty()
  user: User;

  @IsNotEmpty()
  notification: Notification;

  @IsNotEmpty()
  meeting: Meeting;
}
