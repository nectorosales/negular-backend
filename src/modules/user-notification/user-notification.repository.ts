import { EntityRepository, getConnection, Repository } from 'typeorm';
import { UserNotification } from './user-notification.entity';
import { plainToClass } from 'class-transformer';
import { HttpException, HttpStatus } from '@nestjs/common';

@EntityRepository(UserNotification)
export class UserNotificationRepository extends Repository<UserNotification>{

  async getAllByUser(params: any) {
    try {
      const {userId, id} = params;
      return plainToClass(UserNotification, await getConnection()
        .createQueryBuilder()
        .select("user_notification")
        .from(UserNotification, "user_notification")
        .innerJoinAndSelect("user_notification.meeting", "meeting")
        .innerJoinAndSelect("meeting.event", "event")
        .innerJoinAndSelect("user_notification.notification", "notification")
        .where("event.user_id = :userId", {userId: userId})
        .where("user_notification.user_id = :user_id", {user_id: id})
        .getMany()
      );
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }
}
