import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '../../shared/shared.module';
import { AuthModule } from '../auth/auth.module';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';
import { UserNotificationRepository } from './user-notification.repository';
import { UserNotificationService } from './user-notification.service';
import { UserNotificationController } from './user-notification.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserNotificationRepository]),
    SharedModule,
    AuthModule
  ],
  providers: [
    UserNotificationService,
    AuthHelperService
  ],
  controllers: [
    UserNotificationController
  ]
})
export class UserNotificationModule {}
