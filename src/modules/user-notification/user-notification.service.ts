import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';
import { UserNotificationRepository } from './user-notification.repository';

@Injectable()
export class UserNotificationService {
  constructor(
    @InjectRepository(UserNotificationRepository)
    private readonly _userNotificationRepository: UserNotificationRepository,
    private readonly _authHelperService: AuthHelperService
  ) {}

  async getAllByUser(params) {
    return await this._userNotificationRepository.getAllByUser(params);
  }

}
