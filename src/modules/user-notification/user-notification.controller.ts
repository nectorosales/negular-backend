import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { UserNotificationService } from './user-notification.service';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../role/decorators/role.decorator';
import { RoleType } from '../role/roletype.enum';

@Controller('user-notifications')
export class UserNotificationController {
  constructor(private readonly _userNotificationService: UserNotificationService) {}

  @Get(':userId/user/:id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getAllByUser(@Param() {userId, id}: any){
    return await this._userNotificationService.getAllByUser({userId, id});
  }

}
