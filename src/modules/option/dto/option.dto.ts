import { IsNotEmpty } from 'class-validator';
import { Question } from '../../question/question.entity';

export class OptionDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  label: string;

  @IsNotEmpty()
  order: number;

  @IsNotEmpty()
  question: Question;

}
