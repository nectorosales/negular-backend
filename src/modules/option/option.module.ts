import { Module } from '@nestjs/common';
import { OptionRepository } from './option.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OptionService } from './option.service';
import { SharedModule } from '../../shared/shared.module';
import { OptionController } from './option.controller';
import { AuthModule } from '../auth/auth.module';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([OptionRepository]),
    SharedModule,
    AuthModule
  ],
  providers: [
    OptionService,
    AuthHelperService
  ],
  controllers: [
    OptionController
  ]
})
export class OptionModule {}
