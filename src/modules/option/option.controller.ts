import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { OptionService } from './option.service';
import { OptionDto } from './dto/option.dto';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../role/decorators/role.decorator';
import { RoleGuard } from '../role/guards/role.guard';
import { RoleType } from '../role/roletype.enum';
import { Option } from './option.entity';

@Controller('options')
export class OptionController {
  constructor(private readonly _optionService: OptionService) {}

  @Get('question/:id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getOptions(@Param() id: string): Promise<OptionDto[]>{
    return await this._optionService.getAll(id);
  }

  @Get(':id')
  @UseGuards(AuthGuard(), RoleGuard)
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getEvent(@Param('id') id: string): Promise<OptionDto> {
    return await this._optionService.get(id);
  }

  @Post()
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async create(@Body() option: Option): Promise<OptionDto> {
    return await this._optionService.store(option);
  }

  @Put(':id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async update(@Param('id') id: string, @Body() option: Option): Promise<OptionDto> {
    return await this._optionService.update(id, option);
  }

  @Delete()
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async delete(@Query() query) {
    return this._optionService.destroy(query.ids);
  }
}
