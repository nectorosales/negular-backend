import { Injectable } from '@nestjs/common';
import { OptionRepository } from './option.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { OptionDto } from './dto/option.dto';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';
import { Option } from './option.entity';
import { plainToClass } from 'class-transformer';

@Injectable()
export class OptionService {
  constructor(
    @InjectRepository(OptionRepository)
    private readonly _optionRepository: OptionRepository,
    private readonly _authHelperService: AuthHelperService
  ) {}

  async getAll(idQuestion: string): Promise<OptionDto[]>{
    return plainToClass(OptionDto, await this._optionRepository.find({where: {question: idQuestion}, order: { order: 'ASC'}}));
  }

  async get(id: string): Promise<OptionDto>{
    return this._authHelperService.runAwaitGet(this._optionRepository, Option, id);
  }

  async store(availability: Partial<Option>): Promise<OptionDto>{
    return this._authHelperService.runAwaitPost(this._optionRepository, Option, availability);
  }

  async update(id: string, option: Partial<Option>): Promise<OptionDto>{
    return this._authHelperService.runAwaitPath(this._optionRepository, Option, option, id);
  }

  async destroy(ids: Array<string>) {
    return this._authHelperService.runAwaitDelete(this._optionRepository, ids);
  }
}
