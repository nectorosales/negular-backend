import {
  BaseEntity,
  Column, CreateDateColumn, DeleteDateColumn,
  Entity, JoinColumn, ManyToOne, OneToOne,
  PrimaryGeneratedColumn, UpdateDateColumn,
} from 'typeorm';
import { Question } from '../question/question.entity';
import { Event } from '../event/event.entity';

@Entity('options')
export class Option extends BaseEntity {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({type: 'varchar', length: 150, nullable: true })
  label: string;

  @Column({type: 'int', nullable: false})
  order: number;

  @ManyToOne(type => Question, question => question.options, {onDelete: 'CASCADE'})
  @JoinColumn({name: 'question_id'})
  question: Question;

  @CreateDateColumn({type: 'timestamp', name: 'created_at'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamp', name: 'updated_at'})
  updatedAt: Date;

  @DeleteDateColumn({type: 'timestamp', name: 'deleted_at'})
  deletedAt: Date;

}
