import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { NotificationService } from './notification.service';
import { NotificationDto } from './dto/notification.dto';
import { Notification } from './notification.entity';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../role/decorators/role.decorator';
import { RoleGuard } from '../role/guards/role.guard';
import { RoleType } from '../role/roletype.enum';

@Controller('notifications')
export class NotificationController {
  constructor(private readonly _notificationService: NotificationService) {}

  @Get('user/:id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getNotifications(@Param() idUser: number): Promise<NotificationDto[]>{
    return await this._notificationService.getAllByUser(idUser);
  }

  @Get(':id')
  @UseGuards(AuthGuard(), RoleGuard)
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getNotification(@Param('id') id: string): Promise<NotificationDto> {
    return await this._notificationService.get(id);
  }

  @Post('sendEmail')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async sendEmail(@Body() body) {
    return await this._notificationService.sendEmail(body);
  }

  @Post()
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async create(@Body() notification: Notification): Promise<NotificationDto> {
    return await this._notificationService.store(notification);
  }

  @Put(':id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async update(@Param('id') id: string, @Body() notification: Notification) {
    return await this._notificationService.update(id, notification);
  }

  @Delete()
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async delete(@Query() query) {
    return this._notificationService.destroy(query.ids);
  }

}
