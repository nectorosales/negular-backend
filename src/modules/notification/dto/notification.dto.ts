import { IsNotEmpty } from 'class-validator';
import { User } from '../../user/user.entity';

export class NotificationDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  subject: string;

  @IsNotEmpty()
  body: string;

  @IsNotEmpty()
  user: User;

}
