import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { NotificationRepository } from './notification.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { NotificationDto } from './dto/notification.dto';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';
import { Notification } from './notification.entity';
import { plainToClass } from 'class-transformer';
import { MailerService } from '@nestjs-modules/mailer';
import { UserNotificationRepository } from '../user-notification/user-notification.repository';

@Injectable()
export class NotificationService {
  constructor(
    @InjectRepository(NotificationRepository)
    private readonly _notificationRepository: NotificationRepository,
    private readonly _authHelperService: AuthHelperService,
    private readonly mailerService: MailerService,
    @InjectRepository(UserNotificationRepository)
    private readonly _userNotificationRepository: UserNotificationRepository,
  ) {}

  async getAllByUser(idUser: number): Promise<NotificationDto[]>{
    return plainToClass(NotificationDto, await this._notificationRepository.find({where: {user: idUser}}));
  }

  async get(id: string): Promise<NotificationDto>{
    return this._authHelperService.runAwaitGet(this._notificationRepository, Notification, id);
  }

  async store(notification: Partial<Notification>): Promise<NotificationDto>{
    return this._authHelperService.runAwaitPost(this._notificationRepository, Notification, notification);
  }

  async update(id: string, notification: Partial<Notification>): Promise<NotificationDto>{
    return plainToClass(NotificationDto, await this._notificationRepository.save(notification));
    //return this._authHelperService.runAwaitPath(this._notificationRepository, Notification, notification, id);
  }

  async destroy(ids: Array<number>) {
    return this._authHelperService.runAwaitDelete(this._notificationRepository, ids);
  }

  async sendEmail(body: any) {

    let bcc = [];
    body.users.forEach(u => {
      this._userNotificationRepository.save({
        user: u.id, notification: body.notification.id, meeting: body.meeting_id, subject: body.notification.subject, body: body.notification.body
      })
      bcc.push(u.email);
    });

    this
      .mailerService
      .sendMail({
        bcc: bcc, // List of receivers email address
        from: 'noreply@negular.com', // Senders email address
        subject: body.notification.subject, // Subject line
        html: body.notification.body, // HTML body content
      })
      .then((success) => {
        return {
          message: 'Email sent successfully'
        };
      })
      .catch((err) => {
        throw new HttpException(err, HttpStatus.BAD_REQUEST);
      });
  }
}
