import {
  BaseEntity,
  Column, CreateDateColumn, DeleteDateColumn,
  Entity, JoinColumn,
  ManyToOne, OneToMany,
  PrimaryGeneratedColumn, UpdateDateColumn,
} from 'typeorm';
import { User } from '../user/user.entity';
import { UserNotification } from '../user-notification/user-notification.entity';

@Entity('notifications')
export class Notification extends BaseEntity {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({type: 'varchar', length: 255, nullable: false})
  name: string;

  @Column({type: 'varchar', length: 255, nullable: false})
  subject: string;

  @Column({type: 'varchar', length: 500, nullable: false})
  body: string;

  @ManyToOne(type => User)
  @JoinColumn({name: 'user_id'})
  user: User;

  /*@ManyToMany(type => User, user => user.notifications)
  @JoinColumn()
  users: User[];*/

  @OneToMany(type => UserNotification, userNotifications => userNotifications.user)
  userNotifications: UserNotification[];

  @CreateDateColumn({type: 'timestamp', name: 'created_at'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamp', name: 'updated_at'})
  updatedAt: Date;

  @DeleteDateColumn({type: 'timestamp', name: 'deleted_at'})
  deletedAt: Date;
}
