import { Module } from '@nestjs/common';
import { NotificationRepository } from './notification.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NotificationService } from './notification.service';
import { SharedModule } from '../../shared/shared.module';
import { NotificationController } from './notification.controller';
import { AuthModule } from '../auth/auth.module';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';
import { UserNotificationRepository } from '../user-notification/user-notification.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([NotificationRepository, UserNotificationRepository]),
    SharedModule,
    AuthModule
  ],
  providers: [
    NotificationService,
    AuthHelperService
  ],
  controllers: [
    NotificationController
  ]
})
export class NotificationModule {}
