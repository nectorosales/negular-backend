import {
  BaseEntity,
  Column, CreateDateColumn, DeleteDateColumn,
  Entity, JoinColumn, JoinTable,
  ManyToOne, OneToMany,
  PrimaryGeneratedColumn, UpdateDateColumn,
} from 'typeorm';
import { User } from '../user/user.entity';
import { Meeting } from '../meeting/meeting.entity';

@Entity('rooms')
export class Room extends BaseEntity {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({type: 'varchar', length: 150, nullable: false})
  name: string;

  @Column({type: 'varchar', length: 50, nullable: false})
  code: string;

  @Column({type: 'boolean', default: false})
  active: boolean;

  @ManyToOne(type => User, user => user.rooms)
  @JoinColumn({name: 'user_id'})
  user: User;

 /* @OneToMany(type => Meeting, meeting => meeting.room)
  @JoinTable({name: 'meetings'})
  meetings: Meeting[];*/

  @CreateDateColumn({type: 'timestamp', name: 'created_at'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamp', name: 'updated_at'})
  updatedAt: Date;

  @DeleteDateColumn({type: 'timestamp', name: 'deleted_at'})
  deletedAt: Date;
}
