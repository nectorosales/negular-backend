import { Injectable } from '@nestjs/common';
import { RoomRepository } from './room.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { RoomDto } from './dto/room.dto';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';
import { plainToClass } from 'class-transformer';
import { Room } from './room.entity';

@Injectable()
export class RoomService {
  constructor(
    @InjectRepository(RoomRepository)
    private readonly _roomRepository: RoomRepository,
    private readonly _authHelperService: AuthHelperService
  ) {}

  async getByCode(id: string, code: string): Promise<RoomDto>{
    return plainToClass(RoomDto, await this._roomRepository.getByCode(id, code));
  }

  async getAllByUser(idUser: string): Promise<RoomDto[]>{
    return plainToClass(RoomDto, await this._roomRepository.findByUser(idUser));
  }

  async get(id: string): Promise<RoomDto>{
    return this._authHelperService.runAwaitGet(this._roomRepository, Room, id);
  }

  async store(room: Partial<Room>): Promise<RoomDto>{
    return this._authHelperService.runAwaitPost(this._roomRepository, Room, room);
  }

  async update(id: string, room: Partial<Room>): Promise<RoomDto>{
    return this._authHelperService.runAwaitPath(this._roomRepository, Room, room, id);
  }

  async destroy(ids: Array<number>) {
    return this._authHelperService.runAwaitDelete(this._roomRepository, ids);
  }
}
