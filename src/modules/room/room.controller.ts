import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { RoomService } from './room.service';
import { RoomDto } from './dto/room.dto';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../role/decorators/role.decorator';
import { RoleGuard } from '../role/guards/role.guard';
import { RoleType } from '../role/roletype.enum';
import { Room } from './room.entity';

@Controller('rooms')
export class RoomController {
  constructor(private readonly _roomService: RoomService) {}
  
  @Get('user/:id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getAllRoomUser(@Param() id: string): Promise<RoomDto[]>{
    return await this._roomService.getAllByUser(id);
  }

  @Get(':id/:code')
  async getByCode(@Param() id: string, @Param() code: string): Promise<RoomDto>{
    return await this._roomService.getByCode(id, code);
  }

  @Get(':id')
  @UseGuards(AuthGuard(), RoleGuard)
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getRoom(@Param('id') id: string): Promise<RoomDto> {
    return await this._roomService.get(id);
  }

  @Post()
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async create(@Body() room: Room): Promise<RoomDto> {
    return await this._roomService.store(room);
  }

  @Put(':id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async update(@Param('id') id: string, @Body() room: Room) {
    return await this._roomService.update(id, room);
  }

  @Delete()
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async delete(@Query() query) {
    return this._roomService.destroy(query.ids);
  }
}
