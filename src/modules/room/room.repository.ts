import { EntityRepository, getRepository, Repository } from 'typeorm';
import { plainToClass } from 'class-transformer';
import { RoomDto } from './dto/room.dto';
import { ConflictException, HttpException, HttpStatus } from '@nestjs/common';
import { Room } from './room.entity';

@EntityRepository(Room)
export class RoomRepository extends Repository<Room>{

  async findByUser(param: any) {
    try {
      return plainToClass(RoomDto, await getRepository(Room).find({
        where: {
          user: param.id
        }
      }));
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

  async getByCode(id: string, code: string){
    try {
      const room = plainToClass(RoomDto, await getRepository(Room).findOne({where: id}));
      if(room){
        return room;
      } else {
        throw new ConflictException("wrong id or code");
      }
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }
}
