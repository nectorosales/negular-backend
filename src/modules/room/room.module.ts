import { Module } from '@nestjs/common';
import { RoomRepository } from './room.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoomService } from './room.service';
import { SharedModule } from '../../shared/shared.module';
import { RoomController } from './room.controller';
import { AuthModule } from '../auth/auth.module';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([RoomRepository]),
    SharedModule,
    AuthModule
  ],
  providers: [
    RoomService,
    AuthHelperService
  ],
  controllers: [
    RoomController
  ]
})
export class RoomModule {}
