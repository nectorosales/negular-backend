import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';
import { User } from '../../user/user.entity';

export class RoomDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  code: string;

  @IsBoolean()
  @IsNotEmpty()
  active: boolean;

  @IsNotEmpty()
  user: User;
}
