import { Injectable } from '@nestjs/common';
import { RoleRepository } from './role.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from './role.entity';
import { CreateRoleDto, ReadRoleDto, UpdateRoleDto } from './dtos';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(RoleRepository)
    private readonly _roleRepository: RoleRepository,
    private readonly _authHelperService: AuthHelperService,
  ) {
  }

  async getAll(): Promise<ReadRoleDto[]>{
    return this._authHelperService.runAwaitGetAll(this._roleRepository);
  }

  async get(id: string): Promise<ReadRoleDto>{
    return this._authHelperService.runAwaitGet(this._roleRepository, Role, id);
  }

  async store(role: Partial<CreateRoleDto>): Promise<ReadRoleDto>{
    return this._authHelperService.runAwaitPost(this._roleRepository, Role, role);
  }

  async update(id: string, role: Partial<UpdateRoleDto>): Promise<ReadRoleDto>{
    return this._authHelperService.runAwaitPath(this._roleRepository, Role, role, id);
  }

  async destroy(id: string): Promise<boolean> {
    return this._authHelperService.runAwaitDelete(this._roleRepository,  id);
  }
}
