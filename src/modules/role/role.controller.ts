import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { RoleService } from './role.service';
import { CreateRoleDto, ReadRoleDto, UpdateRoleDto } from './dtos';

@Controller('roles')
export class RoleController {
  constructor(private readonly _roleService: RoleService) {

  }

  @Get()
  getRoles(@Param() roleId: number): Promise<ReadRoleDto[]>{
    return this._roleService.getAll();
  }

  @Get(':roleId')
  getRole(@Param('roleId') roleId: string): Promise<ReadRoleDto> {
    return this._roleService.get(roleId);
  }

  @Post()
  create(@Body() role: Partial<CreateRoleDto>): Promise<ReadRoleDto> {
    return this._roleService.store(role);
  }

  @Put(':roleId')
  update(@Param('roleId') roleId: string, @Body() role: Partial<UpdateRoleDto>): Promise<ReadRoleDto>{
    return this._roleService.update(roleId, role);
  }

  @Delete(':roleId')
  delete(@Param('roleId') roleId: string) {
    return this._roleService.destroy(roleId);
  }
}
