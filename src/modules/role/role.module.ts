import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '../../shared/shared.module';
import { MapperService } from '../../shared/mapper.service';
import { RoleRepository } from './role.repository';
import { RoleService } from './role.service';
import { RoleController } from './role.controller';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([RoleRepository]),
    SharedModule],
  providers: [
    RoleService,
    MapperService,
    AuthHelperService
  ],
  controllers: [
    RoleController
  ]
})
export class RoleModule {

}
