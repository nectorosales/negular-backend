import { IsNotEmpty } from 'class-validator';
import { Event } from '../../event/event.entity';

export class QuestionDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  label: string;

  @IsNotEmpty()
  type: string;

  @IsNotEmpty()
  order: number;

  @IsNotEmpty()
  event: Event;

}
