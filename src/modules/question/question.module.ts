import { Module } from '@nestjs/common';
import { QuestionRepository } from './question.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { QuestionService } from './question.service';
import { SharedModule } from '../../shared/shared.module';
import { QuestionController } from './question.controller';
import { AuthModule } from '../auth/auth.module';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([QuestionRepository]),
    SharedModule,
    AuthModule
  ],
  providers: [
    QuestionService,
    AuthHelperService
  ],
  controllers: [
    QuestionController
  ]
})
export class QuestionModule {}
