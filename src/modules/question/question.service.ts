import { Injectable } from '@nestjs/common';
import { QuestionRepository } from './question.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { QuestionDto } from './dto/question.dto';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';
import { Question } from './question.entity';
import { plainToClass } from 'class-transformer';

@Injectable()
export class QuestionService {
  constructor(
    @InjectRepository(QuestionRepository)
    private readonly _questionRepository: QuestionRepository,
    private readonly _authHelperService: AuthHelperService
  ) {}

  async getAll(idEvent: string): Promise<QuestionDto[]>{
    return plainToClass(QuestionDto, await this._questionRepository.find({where: {event: idEvent}, order: { order: 'ASC'}}));
  }

  async get(id: string): Promise<QuestionDto>{
    return this._authHelperService.runAwaitGet(this._questionRepository, Question, id);
  }

  async store(availability: Partial<Question>): Promise<QuestionDto>{
    return this._authHelperService.runAwaitPost(this._questionRepository, Question, availability);
  }

  async update(id: string, option: Partial<Question>): Promise<QuestionDto>{
    return this._authHelperService.runAwaitPath(this._questionRepository, Question, option, id);
  }

  async destroy(ids: Array<string>) {
    return this._authHelperService.runAwaitDelete(this._questionRepository, ids);
  }
}
