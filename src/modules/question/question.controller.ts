import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { QuestionService } from './question.service';
import { QuestionDto } from './dto/question.dto';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../role/decorators/role.decorator';
import { RoleGuard } from '../role/guards/role.guard';
import { RoleType } from '../role/roletype.enum';
import { Question } from './question.entity';

@Controller('questions')
export class QuestionController {
  constructor(private readonly _questionService: QuestionService) {}

  @Get('event/:id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getQuestions(@Param() id: string): Promise<QuestionDto[]>{
    return await this._questionService.getAll(id);
  }

  @Get(':id')
  @UseGuards(AuthGuard(), RoleGuard)
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getEvent(@Param('id') id: string): Promise<QuestionDto> {
    return await this._questionService.get(id);
  }

  @Post()
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async create(@Body() question: Question): Promise<QuestionDto> {
    return await this._questionService.store(question);
  }

  @Put(':id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async update(@Param('id') id: string, @Body() question: Question): Promise<QuestionDto> {
    return await this._questionService.update(id, question);
  }

  @Delete()
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async delete(@Query() query) {
    return this._questionService.destroy(query.ids);
  }
}
