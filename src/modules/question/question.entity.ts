import {
  BaseEntity,
  Column, CreateDateColumn, DeleteDateColumn,
  Entity, JoinColumn, JoinTable, ManyToOne, OneToMany,
  PrimaryGeneratedColumn, UpdateDateColumn,
} from 'typeorm';
import { Event } from '../event/event.entity';
import { Option } from '../option/option.entity';

@Entity('questions')
export class Question extends BaseEntity {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({type: 'varchar', length: 150, nullable: true })
  label: string;

  @Column({type: 'varchar', length: 150, nullable: true })
  type: string;

  @Column({type: 'boolean', nullable: false, default: false})
  required: boolean;

  @Column({type: 'int', nullable: false})
  order: number;

  @OneToMany(type => Option, option => option.question, {eager: true})
  @JoinTable({name: 'options'})
  options: Option[];

  @ManyToOne(type => Event, {onDelete: 'CASCADE'})
  @JoinColumn({name: 'event_id'})
  event: Event;

  @CreateDateColumn({type: 'timestamp', name: 'created_at'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamp', name: 'updated_at'})
  updatedAt: Date;

  @DeleteDateColumn({type: 'timestamp', name: 'deleted_at'})
  deletedAt: Date;

}
