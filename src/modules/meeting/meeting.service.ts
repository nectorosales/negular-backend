import { Injectable } from '@nestjs/common';
import { MeetingRepository } from './meeting.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { MeetingDto } from './dto/meeting.dto';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';
import { Meeting } from './meeting.entity';
 import { plainToClass } from 'class-transformer';
import { RoomDto } from '../room/dto/room.dto';
import { UserMeetingRepository } from '../user-meeting/user-meeting.repository';

@Injectable()
export class MeetingService {
  constructor(
    @InjectRepository(MeetingRepository)
    private readonly _meetingRepository: MeetingRepository,
    private readonly _authHelperService: AuthHelperService
  ) {}

  async getAllByEvent(idEvent: string): Promise<MeetingDto[]>{
    return plainToClass(MeetingDto, await this._meetingRepository.find({where: {event: idEvent}}));
  }

  async getAllByUser(idUser: string): Promise<MeetingDto[]>{
    return plainToClass(MeetingDto, await this._meetingRepository.findByUser(idUser));
  }

  async get(id: string): Promise<MeetingDto>{
    return plainToClass(MeetingDto, await this._meetingRepository.show(id));
    /*return plainToClass(MeetingDto, await this._meetingRepository.findOne({
      relations: ['event', 'userMeetings'],
      where: { id: id }
    }));*/
    //return this._authHelperService.runAwaitGet(this._meetingRepository, Meeting, id);
  }

  async store(meeting: Partial<Meeting>): Promise<MeetingDto>{
    return this._authHelperService.runAwaitPost(this._meetingRepository, Meeting, meeting);
  }

  async update(id: string, meeting: Partial<Meeting>): Promise<MeetingDto>{
    return plainToClass(MeetingDto, await this._meetingRepository.save(meeting));
    //return this._authHelperService.runAwaitPath(this._meetingRepository, Meeting, meeting, id);
  }

  async destroy(id: string): Promise<boolean> {
    return this._authHelperService.runAwaitDelete(this._meetingRepository, id);
  }

  async getAllByRoom(idRoom: string) {
    return plainToClass(MeetingDto, await this._meetingRepository.find({where: {room: idRoom}}));
  }

  async getAllRoomMeetingByUser(idUser: string): Promise<MeetingDto[]> {
    return plainToClass(MeetingDto, await this._meetingRepository.findAllRoomMeetingByUser(idUser));
  }
}
