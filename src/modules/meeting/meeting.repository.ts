import { EntityRepository, getConnection, Repository } from 'typeorm';
import { Meeting } from './meeting.entity';
import { plainToClass } from 'class-transformer';
import { MeetingDto } from './dto/meeting.dto';
import { HttpException, HttpStatus, NotFoundException } from '@nestjs/common';

@EntityRepository(Meeting)
export class MeetingRepository extends Repository<Meeting>{

  async findByUser(user: any) {
    try {
      return plainToClass(MeetingDto, await getConnection()
        .createQueryBuilder()
        .select("meeting")
        .from(Meeting, "meeting")
        .leftJoinAndSelect("meeting.event", "event")
        .leftJoinAndSelect("meeting.userMeetings", "users")
        .leftJoinAndSelect("users.user", "user")
        .where("event.user_id = :userId", {userId: user.id})
        .getMany());
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

  async findAllRoomMeetingByUser(idUser) {
    return plainToClass(MeetingDto, await getConnection()
      .createQueryBuilder()
      .select("meeting")
      .from(Meeting, "meeting")
      .leftJoinAndSelect("meeting.event", "event")
      .leftJoinAndSelect("meeting.userMeetings", "users")
      .orderBy("start")
      .getMany());
    try {
      const { id } = idUser;
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

  async show(idMeeting: any): Promise<any> {
    const { id } = idMeeting;
    try {
      const meeting = plainToClass(MeetingDto, await getConnection()
        .createQueryBuilder()
        .select("meeting")
        .from(Meeting, "meeting")
        .leftJoinAndSelect("meeting.event", "event")
        .leftJoinAndMapMany("meeting.userMeetings", "meeting.userMeetings", "userMeeting")
        .leftJoinAndSelect("userMeeting.user", "user")
        .where("meeting.id = :meetingId", {meetingId: idMeeting})
        //.getQueryAndParameters()
        .getOne()
        );
      if(!meeting) throw new NotFoundException();
      return meeting;
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }
}
