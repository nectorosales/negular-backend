import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { MeetingService } from './meeting.service';
import { MeetingDto } from './dto/meeting.dto';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../role/decorators/role.decorator';
import { RoleGuard } from '../role/guards/role.guard';
import { RoleType } from '../role/roletype.enum';
import { Meeting } from './meeting.entity';

@Controller('meetings')
export class MeetingController {
  constructor(private readonly _meetingService: MeetingService) {}

  @Get('event/:id')
  async getMeetingsByEvent(@Param() id: string): Promise<MeetingDto[]>{
    return await this._meetingService.getAllByEvent(id);
  }

  @Get('user/:id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getMeetingsByUser(@Param() id: string): Promise<MeetingDto[]>{
    return await this._meetingService.getAllByUser(id);
  }

  @Get('room/:id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getMeetingsByRoom(@Param() id: string): Promise<MeetingDto[]>{
    return await this._meetingService.getAllByRoom(id);
  }

  @Get('user/:id/meetings')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN, RoleType.CLIENT)
  async getAllRoomMeetingUser(@Param() id: string){
    return await this._meetingService.getAllRoomMeetingByUser(id);
  }

  @Get(':id')
  @UseGuards(AuthGuard(), RoleGuard)
  @Roles(RoleType.USER, RoleType.ADMIN, RoleType.CLIENT)
  async getMeeting(@Param('id') id: string): Promise<MeetingDto> {
    return await this._meetingService.get(id);
  }

  @Post()
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async create(@Body() meeting: Meeting): Promise<MeetingDto> {
    return await this._meetingService.store(meeting);
  }

  @Put(':id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async update(@Param('id') id: string, @Body() meeting: MeetingDto) {
    return await this._meetingService.update(id, meeting);
  }

  @Delete(':id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async delete(@Param('id') id: string) {
    return this._meetingService.destroy(id);
  }

}
