import { IsBoolean, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { Event } from '../../event/event.entity';
import { UserMeeting } from '../../user-meeting/user-meeting.entity';

export class MeetingDto {
  @IsNotEmpty()
  id: string;

  @IsString()
  description: string;

  @IsNotEmpty()
  start: Date;

  @IsNotEmpty()
  end: Date;

  @IsNotEmpty()
  @IsBoolean()
  active: boolean;

  @IsNotEmpty()
  @IsString()
  state: string;

  @IsNotEmpty()
  event: Event;

/*  @IsNotEmpty()
  userMeetings: UserMeeting[];*/
}
