import { Module } from '@nestjs/common';
import { MeetingRepository } from './meeting.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MeetingService } from './meeting.service';
import { SharedModule } from '../../shared/shared.module';
import { MeetingController } from './meeting.controller';
import { AuthModule } from '../auth/auth.module';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([MeetingRepository]),
    SharedModule,
    AuthModule
  ],
  providers: [
    MeetingService,
    AuthHelperService
  ],
  controllers: [
    MeetingController
  ]
})
export class MeetingModule {}
