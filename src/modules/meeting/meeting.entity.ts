import {
  BaseEntity,
  Column, CreateDateColumn, DeleteDateColumn,
  Entity, JoinColumn, ManyToMany,
  ManyToOne, OneToMany,
  PrimaryGeneratedColumn, UpdateDateColumn,
} from 'typeorm';
import { Event } from '../event/event.entity';
import { User } from '../user/user.entity';
import { UserNotification } from '../user-notification/user-notification.entity';
import { UserMeeting } from '../user-meeting/user-meeting.entity';

@Entity('meetings')
export class Meeting extends BaseEntity {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({type: 'text', nullable: true})
  description: string;

  @Column({type: 'datetime', nullable: false})
  start: Date;

  @Column({type: 'datetime', nullable: false})
  end: Date;

  @ManyToOne(type => Event, event => event.meetings)
  @JoinColumn({name: 'event_id'})
  event: Event;

 /* @ManyToOne(type => Room, room => room.meetings, {eager: true, onDelete: 'CASCADE'})
  @JoinColumn({name: 'room_id'})
  room: Room;*/

  @Column({type: 'boolean', default: false})
  active: boolean;

  @Column({type: 'varchar', default: 'pending'})
  state: string;

  /*@ManyToMany(type => User, user => user.meetings, {eager: true})
  @JoinColumn()
  users: User[];*/

  @OneToMany(type => UserNotification, userNotifications => userNotifications.meeting, {eager: true})
  userNotifications: UserNotification[];

  @OneToMany(type => UserMeeting, userMeetings => userMeetings.meeting, {eager: true})
  userMeetings: UserMeeting[];

  @CreateDateColumn({type: 'timestamp', name: 'created_at'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamp', name: 'updated_at'})
  updatedAt: Date;

  @DeleteDateColumn({type: 'timestamp', name: 'deleted_at'})
  deletedAt: Date;
}
