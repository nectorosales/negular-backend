import { Injectable } from '@nestjs/common';
import { AvailabilityRepository } from './availability.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { AvailabilityDto } from './dto/availability.dto';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';
import { Availability } from './availability.entity';
import { plainToClass } from 'class-transformer';

@Injectable()
export class AvailabilityService {
  constructor(
    @InjectRepository(AvailabilityRepository)
    private readonly _availabilityRepository: AvailabilityRepository,
    private readonly _authHelperService: AuthHelperService
  ) {}

  async getAll(idEvent: string): Promise<AvailabilityDto[]>{
    return plainToClass(AvailabilityDto, await this._availabilityRepository.find({where: {event: idEvent}, order: { date: 'ASC'}}));
  }

  async get(id: string): Promise<AvailabilityDto>{
    return this._authHelperService.runAwaitGet(this._availabilityRepository, Availability, id);
  }

  async store(availability: Partial<Availability>): Promise<AvailabilityDto>{
    return this._authHelperService.runAwaitPost(this._availabilityRepository, Availability, availability);
  }

  async update(id: string, availability: Partial<Availability>): Promise<AvailabilityDto>{
    return this._authHelperService.runAwaitPath(this._availabilityRepository, Availability, availability, id);
  }

  async destroy(ids: Array<number>) {
    return this._authHelperService.runAwaitDelete(this._availabilityRepository, ids);
  }
}
