import {
  BaseEntity,
  Column, CreateDateColumn, DeleteDateColumn,
  Entity, JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn, UpdateDateColumn,
} from 'typeorm';
import {Event} from '../event/event.entity';

@Entity('availabilities')
export class Availability extends BaseEntity {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({type: 'date', nullable: true })
  date: Date;

  @Column({type: 'tinyint', nullable: false})
  day: number;

  @Column({type: 'time', nullable: false})
  begin: Date;

  @Column({type: 'time', nullable: false})
  end: Date;

  @Column({type: 'boolean', nullable: false, default: false})
  always: boolean;

  @ManyToOne(type => Event, {onDelete: 'CASCADE'})
  @JoinColumn({name: 'event_id'})
  event: Event;

  @CreateDateColumn({type: 'timestamp', name: 'created_at'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamp', name: 'updated_at'})
  updatedAt: Date;

  @DeleteDateColumn({type: 'timestamp', name: 'deleted_at'})
  deletedAt: Date;
}
