import { Module } from '@nestjs/common';
import { AvailabilityRepository } from './availability.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AvailabilityService } from './availability.service';
import { SharedModule } from '../../shared/shared.module';
import { AvailabilityController } from './availability.controller';
import { AuthModule } from '../auth/auth.module';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([AvailabilityRepository]),
    SharedModule,
    AuthModule
  ],
  providers: [
    AvailabilityService,
    AuthHelperService
  ],
  controllers: [
    AvailabilityController
  ]
})
export class AvailabilityModule {}
