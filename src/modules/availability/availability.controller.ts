import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { AvailabilityService } from './availability.service';
import { AvailabilityDto } from './dto/availability.dto';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../role/decorators/role.decorator';
import { RoleGuard } from '../role/guards/role.guard';
import { RoleType } from '../role/roletype.enum';
import { Availability } from './availability.entity';

@Controller('availabilities')
export class AvailabilityController {
  constructor(private readonly _eventService: AvailabilityService) {}

  @Get('event/:id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getEvents(@Param() id: string): Promise<AvailabilityDto[]>{
    return await this._eventService.getAll(id);
  }

  @Get(':id')
  @UseGuards(AuthGuard(), RoleGuard)
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getEvent(@Param('id') id: string): Promise<AvailabilityDto> {
    return await this._eventService.get(id);
  }

  @Post()
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async create(@Body() availability: Availability): Promise<AvailabilityDto> {
    return await this._eventService.store(availability);
  }

  @Put(':id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async update(@Param('id') id: string, @Body() availability: Availability): Promise<AvailabilityDto> {
    return await this._eventService.update(id, availability);
  }

  @Delete()
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async delete(@Query() query) {
    return this._eventService.destroy(query.ids);
  }
}
