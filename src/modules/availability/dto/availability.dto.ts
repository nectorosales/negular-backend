import { IsNotEmpty } from 'class-validator';
import { Event } from '../../event/event.entity';

export class AvailabilityDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  date: Date;

  @IsNotEmpty()
  day: number;

  @IsNotEmpty()
  begin: Date;

  @IsNotEmpty()
  end: Date;

  @IsNotEmpty()
  always: boolean;

  @IsNotEmpty()
  event: Event;
}
