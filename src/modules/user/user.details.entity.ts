import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('user_details')
export class UserDetails extends BaseEntity {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({type: 'varchar', length: 255, nullable: false})
  img: string;

  @Column({type: 'varchar', length: 50, nullable: true})
  name: string;

  @Column({type: 'varchar', nullable: true})
  lastName: string;

  @Column({type: 'int', nullable: true})
  phone: number;

  @Column({type: 'boolean', default: false})
  activeBizum: boolean;

  @CreateDateColumn({type: 'timestamp', name: 'created_at'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamp', name: 'updated_at'})
  updatedAt: Date;

  @DeleteDateColumn({type: 'timestamp', name: 'deleted_at'})
  deletedAt: Date;
}
