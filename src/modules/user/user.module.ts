import { Module } from '@nestjs/common';
import { UserRepository } from './user.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from './user.service';
import { SharedModule } from '../../shared/shared.module';
import { UserController } from './user.controller';
import { MapperService } from '../../shared/mapper.service';
import { AuthModule } from '../auth/auth.module';
import { RoleRepository } from '../role/role.repository';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserRepository, RoleRepository]),
    SharedModule,
    AuthModule
  ],
  providers: [
    UserService,
    MapperService,
    AuthHelperService
  ],
  controllers: [
    UserController
  ]
})
export class UserModule {}
