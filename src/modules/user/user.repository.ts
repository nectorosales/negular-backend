import { EntityRepository, getConnection, Repository } from 'typeorm';
import { User } from './user.entity';
import { plainToClass } from 'class-transformer';
import { HttpException, HttpStatus } from '@nestjs/common';
import { UserDto } from './dto/user.dto';
import { RoleRepository } from '../role/role.repository';
import { Role } from '../role/role.entity';
import { RoleType } from '../role/roletype.enum';
import { UserDetails } from './user.details.entity';
import { genSalt, hash } from 'bcryptjs';

@EntityRepository(User)
export class UserRepository extends Repository<UserDto>{

  async createOrUpdate(request, user) {

    const { email, password, name, lastName, phone, role } = request;

    const roleRepository: RoleRepository = await getConnection().getRepository(
      Role,
    );

    const roleExists: Role = await roleRepository.findOne({ where: { name: role ? role : RoleType.CLIENT } })

    if(!user) {
      const user = new User();
      user.email = email;

      user.roles = [roleExists];

      const details = new UserDetails();
      details.name = name ? name : null;
      details.lastName = lastName ? lastName : null;
      details.phone = phone ? phone : null;
      user.details = details;

      const salt = await genSalt(10);
      user.password = await hash(password, salt);

      return await user.save();

    } else {
      user.roles.push(roleExists);
      return await user.save();
    }
  }
}
