import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { UserRepository } from './user.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { MapperService } from '../../shared/mapper.service';
import { UserDto } from './dto/user.dto';
import { User } from './user.entity';
import { RoleRepository } from '../role/role.repository';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';
import { Role } from '../role/role.entity';
import { plainToClass } from 'class-transformer';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private readonly _userRepository: UserRepository,
    @InjectRepository(RoleRepository)
    private readonly _roleRepository: RoleRepository,
    private readonly _mapperService: MapperService,
    private readonly _authHelperService: AuthHelperService
  ) {}

  async getAll(): Promise<UserDto[]>{
    return this._authHelperService.runAwaitGetAll(this._userRepository);
  }

  async get(id: string): Promise<UserDto>{
    return this._authHelperService.runAwaitGet(this._userRepository, User, id);
  }

  async createOrUpdate(request): Promise<any>{
    const {email} = request;
    const userExists = await this._userRepository.findOne({
      where: [{email}],
    });

    const user = await this._userRepository.createOrUpdate(request, userExists);

    return user;
  }

  async update(id: string, user): Promise<UserDto>{

    const { img, name, lastName, phone, activeBizum } = user;
    const userExists = await this._userRepository.findOne({
      where: [{id}],
    });

    userExists.details.img = img;
    userExists.details.name = name;
    userExists.details.lastName = lastName;
    userExists.details.phone = phone;
    userExists.details.activeBizum = activeBizum;

    return plainToClass(UserDto, await this._userRepository.save(userExists));

  }

  async destroy(id: string): Promise<boolean> {
    return this._authHelperService.runAwaitDelete(this._userRepository, id);
  }

  async setRoleToUser(userId: number, roleId: number){
    const userExist = await this._authHelperService.runAwaitGet(this._userRepository, User, userId);
    const roleExist = await this._authHelperService.runAwaitGet(this._roleRepository, Role, roleId);
    return this._authHelperService.runAwaitRelationQueryPost(User, "roles", userExist, roleExist);
  }

  async outRoleToUser(userId: number, roleId: number) {
    const userExist = await this._authHelperService.runAwaitGet(this._userRepository, User, userId);
    const roleExist = await this._authHelperService.runAwaitGet(this._roleRepository, Role, roleId);
    return this._authHelperService.runAwaitRelationQueryDelete(User, "roles", userExist, roleExist);
  }

  async getUsername(username: string): Promise<UserDto>{
    const user = await this._userRepository.findOne({
      where: {username: username}
    });
    if(!user) throw new NotFoundException();
    return plainToClass(UserDto, user);
  }
}
