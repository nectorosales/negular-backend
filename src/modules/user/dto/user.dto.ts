import { IsNotEmpty, IsString } from 'class-validator';
import { UserDetails } from '../user.details.entity';
import { RoleType } from '../../role/roletype.enum';
import { Event } from '../../event/event.entity';

export class UserDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  @IsString()
  username: string;

  @IsNotEmpty()
  @IsString()
  email: string;

  @IsNotEmpty()
  @IsString()
  password: string;

  @IsNotEmpty()
  roles: RoleType[];

  @IsNotEmpty()
  meetings: RoleType[];

  @IsNotEmpty()
  events: Event[];

  @IsNotEmpty()
  details: UserDetails;
}
