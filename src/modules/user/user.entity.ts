import {
  BaseEntity,
  Column, CreateDateColumn, DeleteDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany, OneToMany,
  OneToOne,
  PrimaryGeneratedColumn, UpdateDateColumn,
} from 'typeorm';
import { UserDetails } from './user.details.entity';
import { Role } from '../role/role.entity';
import { Room } from '../room/room.entity';
import { Meeting } from '../meeting/meeting.entity';
import { Event } from '../event/event.entity';
import { UserNotification } from '../user-notification/user-notification.entity';
import { UserMeeting } from '../user-meeting/user-meeting.entity';
import { Exclude } from 'class-transformer';

@Entity('users')
export class User extends BaseEntity {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({type: 'varchar', length: 100, nullable: false, unique: true})
  username: string;

  @Column({type: 'varchar', length: 25, nullable: false})
  email: string;

  @Column({type: 'varchar', nullable: false})
  @Exclude()
  password: string;

  @OneToOne(type => UserDetails, {cascade: true, nullable: false, eager: true, onDelete: 'CASCADE'})
  @JoinColumn({name: 'detail_id'})
  details: UserDetails;

  @ManyToMany(type => Role, role => role.users, {eager: true})
  @JoinTable({name: 'user_roles'})
  roles: Role[];

  /*@ManyToMany(type => Meeting, meeting => meeting.users)
  @JoinTable({name: 'user_meetings'})
  meetings: Meeting[];*/

  @OneToMany(type => Room, room => room.user, {eager: true})
  @JoinTable({name: 'rooms'})
  rooms: Room[];

 /* @ManyToMany(type => Notification, notification => notification.users, {eager: true})
  @JoinTable({ name: 'user_notifications'})
  notifications: Notification[];*/

  @OneToMany(type => UserNotification, userNotifications => userNotifications.user, {eager: true})
  userNotifications: UserNotification[];

  @OneToMany(type => UserMeeting, userMeetings => userMeetings.user)
  userMeetings: UserMeeting[];

  @OneToMany(type => Event, event => event.user, {eager: true})
  @JoinTable({name: 'events'})
  events: Event[];

  @CreateDateColumn({type: 'timestamp', name: 'created_at'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamp', name: 'updated_at'})
  updatedAt: Date;

  @DeleteDateColumn({type: 'timestamp', name: 'deleted_at'})
  deletedAt: Date;
}
