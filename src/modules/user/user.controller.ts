import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.entity';
import { UserDto } from './dto/user.dto';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../role/decorators/role.decorator';
import { RoleGuard } from '../role/guards/role.guard';
import { RoleType } from '../role/roletype.enum';
import { FileInterceptor } from '@nestjs/platform-express';
import { extname } from "path";
import { diskStorage } from 'multer';

@Controller('users')
export class UserController {
  constructor(private readonly _userService: UserService) {}

  @Get()
  async getUsers(): Promise<UserDto[]>{
    return await this._userService.getAll();
  }

  @Get(':id')
  @UseGuards(AuthGuard(), RoleGuard)
  @Roles(RoleType.USER, RoleType.ADMIN, RoleType.CLIENT)
  async getUser(@Param('id') id: string): Promise<UserDto> {
    return await this._userService.get(id);
  }

  @Get('username/:username')
  async getUsername(@Param('username') username: string): Promise<UserDto> {
    return await this._userService.getUsername(username);
  }

  @Post()
  async createOrUpdate(@Body() user): Promise<any> {
    return await this._userService.createOrUpdate(user);
  }

  @Put(':id')
  @UseInterceptors(FileInterceptor('img', {
    storage: diskStorage({
      destination: './public'
      , filename: (req, file, cb) => {
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
        cb(null, `${randomName}${extname(file.originalname)}`)
      }
    })
  }))
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async update(@UploadedFile() file, @Param('id') id: string, @Body() user) {
    if(file) user.img = file.filename;
    return await this._userService.update(id, user);
  }

  @Delete(':id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async delete(@Param('id') id: string) {
    return this._userService.destroy(id);
  }

  @Post('roles/:userId/:roleId')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async setRoleToUser(@Param('userId') userId: number, @Param('roleId') roleId: number) {
    return this._userService.setRoleToUser(userId, roleId);
  }

  @Delete('roles/:userId/:roleId')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async outRoleToUser(@Param('userId') userId: number, @Param('roleId') roleId: number) {
    return this._userService.outRoleToUser(userId, roleId);
  }

}
