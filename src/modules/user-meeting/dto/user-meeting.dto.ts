import { IsNotEmpty, IsString } from 'class-validator';
import { Meeting } from '../../meeting/meeting.entity';
import { User } from '../../user/user.entity';
import { Notification } from '../../notification/notification.entity';

export class UserMeetingDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  @IsString()
  state_payment: string;

  @IsNotEmpty()
  user: User;

  @IsNotEmpty()
  meeting: Meeting;
}
