import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '../../shared/shared.module';
import { AuthModule } from '../auth/auth.module';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';
import { UserMeetingRepository } from './user-meeting.repository';
import { UserMeetingService } from './user-meeting.service';
import { UserMeetingController } from './user-meeting.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserMeetingRepository]),
    SharedModule,
    AuthModule
  ],
  providers: [
    UserMeetingService,
    AuthHelperService
  ],
  controllers: [
    UserMeetingController
  ]
})
export class UserMeetingModule {}
