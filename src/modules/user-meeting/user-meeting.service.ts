import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthHelperService } from '../../shared/auth-helper/auth-helper.service';
import { UserMeetingRepository } from './user-meeting.repository';
import { UserMeetingDto } from './dto/user-meeting.dto';
import { UserMeeting } from './user-meeting.entity';
import { Room } from '../room/room.entity';
import { RoomDto } from '../room/dto/room.dto';

@Injectable()
export class UserMeetingService {
  constructor(
    @InjectRepository(UserMeetingRepository)
    private readonly _userMeetingRepository: UserMeetingRepository,
    private readonly _authHelperService: AuthHelperService
  ) {}

  async getMeetingsByUser(params) {
    return this._userMeetingRepository.getMeetingsByUser(params);
  }

  async store(userMeeting: Partial<UserMeeting>): Promise<UserMeetingDto>{
    return this._authHelperService.runAwaitPost(this._userMeetingRepository, UserMeeting, userMeeting);
  }

  async update(id: string, userMeeting: Partial<UserMeeting>): Promise<UserMeetingDto>{
    return this._authHelperService.runAwaitPath(this._userMeetingRepository, UserMeeting, userMeeting, id);
  }

  async destroy(ids: Array<number>): Promise<boolean> {
    return this._authHelperService.runAwaitDelete(this._userMeetingRepository, ids);
  }

  async getUsersClient(userId) {
    return this._userMeetingRepository.getUsersClient(userId);
  }

  async getPaymentsByUser(id: string) {
    return this._userMeetingRepository.getPaymentsByUser(id);
  }
}
