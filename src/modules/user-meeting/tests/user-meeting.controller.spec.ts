import { Test, TestingModule } from '@nestjs/testing';
import { UserMeetingController } from '../user-meeting.controller';

describe('UserNotification Controller', () => {
  let controller: UserMeetingController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserMeetingController],
    }).compile();

    controller = module.get<UserMeetingController>(UserMeetingController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
