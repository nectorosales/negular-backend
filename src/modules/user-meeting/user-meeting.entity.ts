import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Meeting } from '../meeting/meeting.entity';
import { User } from '../user/user.entity';

@Entity('user_meetings')
export class UserMeeting {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({type: 'varchar', length: 50, nullable: false, default: 'pending'})
  state_payment: string;

  @JoinColumn()
  @ManyToOne(type => User, user => user.userMeetings, {onDelete: 'CASCADE'})
  user: User;

  @JoinColumn()
  @ManyToOne(type => Meeting, meeting => meeting.userMeetings, {onDelete: 'CASCADE'})
  meeting: Meeting;

  @CreateDateColumn({type: 'timestamp', name: 'created_at'})
  createdAt: Date;

  @UpdateDateColumn({type: 'timestamp', name: 'updated_at'})
  updatedAt: Date;

  @DeleteDateColumn({type: 'timestamp', name: 'deleted_at'})
  deletedAt: Date;
}
