import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { UserMeetingService } from './user-meeting.service';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../role/decorators/role.decorator';
import { RoleType } from '../role/roletype.enum';
import { UserMeetingDto } from './dto/user-meeting.dto';
import { UserMeeting } from './user-meeting.entity';
import { Room } from '../room/room.entity';

@Controller('user-meetings')
export class UserMeetingController {
  constructor(private readonly _userMeetingService: UserMeetingService) {}

  @Get(':userId/user/:id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getMeetingsByUser(@Param() {userId, id}: any){
    return await this._userMeetingService.getMeetingsByUser({userId, id});
  }

  @Post()
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async create(@Body() meeting: UserMeeting): Promise<UserMeetingDto> {
    return await this._userMeetingService.store(meeting);
  }

  @Put(':id')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async update(@Param('id') id: string, @Body() userMeeting: UserMeeting) {
    return await this._userMeetingService.update(id, userMeeting);
  }

  @Delete()
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async delete(@Query() query) {
    return this._userMeetingService.destroy(query.ids);
  }

  @Get(':id/clients')
  @UseGuards(AuthGuard())
  async getUsersClient(@Param() id: string){
    return await this._userMeetingService.getUsersClient(id);
  }

  @Get(':id/payments')
  @UseGuards(AuthGuard())
  @Roles(RoleType.USER, RoleType.ADMIN)
  async getPaymentsByUser(@Param() id: string){
    return await this._userMeetingService.getPaymentsByUser(id);
  }

}
