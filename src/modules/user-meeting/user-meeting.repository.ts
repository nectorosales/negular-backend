import { EntityRepository, getConnection, Repository } from 'typeorm';
import { UserMeeting } from './user-meeting.entity';
import { plainToClass } from 'class-transformer';
import { HttpException, HttpStatus } from '@nestjs/common';
import { UserDto } from '../user/dto/user.dto';
import { User } from '../user/user.entity';

@EntityRepository(UserMeeting)
export class UserMeetingRepository extends Repository<UserMeeting>{

  async getUsersClient(userId) {
    try {
      const { id } = userId;
      return plainToClass(UserDto, await getConnection()
        .createQueryBuilder()
        .select('user')
        .from(User, "user")
        .leftJoinAndSelect("user.userMeetings", "userMeeting")
        .leftJoinAndSelect("userMeeting.meeting", "meetingUser")
        .leftJoinAndSelect("meetingUser.event", "meetingEvent")
        .where("meetingEvent.user_id = :user_id", {user_id: id})
        .getMany());
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

  async getMeetingsByUser(params: any) {
    try {
      const {userId, id} = params;
      return plainToClass(UserMeeting, await getConnection()
        .createQueryBuilder()
        .select("user_meeting")
        .from(UserMeeting, "user_meeting")
        .innerJoinAndSelect("user_meeting.meeting", "meeting")
        .innerJoinAndSelect("user_meeting.user", "user", "user_meeting.userId = :user_id", {user_id: id})
        .innerJoinAndSelect("meeting.event", "event", "event.user_id = :userId", {userId: userId})
        .getMany()
      );
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

  async getPaymentsByUser(userId) {
    try {
      const {id} = userId;
      return plainToClass(UserMeeting, await getConnection()
        .createQueryBuilder()
        .select("user_meeting")
        .from(UserMeeting, "user_meeting")
        .innerJoinAndSelect("user_meeting.meeting", "meeting")
        .innerJoinAndSelect("user_meeting.user", "user")
        .innerJoinAndSelect("meeting.event", "event", "event.user_id = :userId", {userId: id})
        .getMany()
      );
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }
}
